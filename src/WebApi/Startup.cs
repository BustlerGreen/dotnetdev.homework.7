using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WebApi
{
    public class Startup
    {
        public readonly IConfiguration _configurtion;
        public Startup(IConfiguration configuration)
        {
            _configurtion= configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            CreateDB();
        }

        void CreateDB()
        {
            var conn = new Npgsql.NpgsqlConnection(_configurtion.GetConnectionString("Postgres"));
            conn.Open();
            var cmd = new Npgsql.NpgsqlCommand();
            cmd.Connection= conn;
            cmd.CommandText = "DROP TABLE IF EXISTS customers";
            cmd.ExecuteNonQuery();
            cmd.CommandText = @"CREATE TABLE customers (
                id BIGINT PRIMARY KEY NOT NULL,
                first_name VARCHAR(128) NOT NULL,
                last_name VARCHAR(128) NOT NULL
                )";
            cmd.ExecuteNonQuery();
            cmd.CommandText = @"INSERT INTO customers (id, first_name, last_name) VALUES (1, 'Peter','Parker')";
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}