using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        public readonly IConfiguration _configuration;
        public CustomerController(IConfiguration configuration) 
        {
            _configuration= configuration;
        }

        [HttpGet("{id:long}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            if (id < 0) return BadRequest();
            var res = await GetCustomerFromBdAsync(id);
            if (null == res) return NotFound();
            return Ok(res);

        }

        [HttpPost("")]   
        public async  Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if(null == customer) return BadRequest();
            var res = await InsertCustomertoBdAsync(customer);
            if (0 > res) return StatusCode(409);
            return Ok(res);
        }

        async Task<Customer> GetCustomerFromBdAsync(long id)
        {
            Customer cust = null;
            var conn = new Npgsql.NpgsqlConnection(_configuration.GetConnectionString("Postgres"));
            await conn.OpenAsync();
            var cmd = new Npgsql.NpgsqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = $@"SELECT id, first_name, last_name FROM customers WHERE id ={id}";
            var dr = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);
            if(await dr.ReadAsync())
            {
                cust = new Customer() {Id = dr.GetInt64(0), Firstname= dr.GetString(1), Lastname = dr.GetString(2)};
            }
            dr.Close();
            conn.Close();
            return cust;
        }

        async Task<long> InsertCustomertoBdAsync(Customer cust)
        {
            long res = -1;
            var conn = new Npgsql.NpgsqlConnection(_configuration.GetConnectionString("Postgres"));
            await conn.OpenAsync();
            var cmd = new Npgsql.NpgsqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = $@"SELECT id FROM customers WHERE id = {cust.Id}";
            var dr = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);
            var exist = dr.HasRows;
            dr.Close();
            if (exist) { conn.Close();  return -1; }
            cmd.CommandText = @$"INSERT INTO customers (id, first_name, last_name) VALUES ({cust.Id}, '{cust.Firstname}', '{cust.Lastname}')
                RETURNING id ";
            dr = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);
            if(await dr.ReadAsync())
            {
                res = dr.GetInt64(0);
            }
            dr.Close();
            conn.Close();
            return res;
        }
    }
}