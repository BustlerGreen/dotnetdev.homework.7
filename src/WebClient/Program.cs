﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var ts = "type customer id (int) for request , q for exit, nc for generate new cutomer";
            Console.WriteLine(ts);
            string rs = string.Empty;
            var Client = new HttpClient();
            
            while (rs != "q")
            {
                rs = Console.ReadLine();
                bool handled = false; 
                if (rs == "nc")
                {
                    handled= true;
                    var cust = RandomCustomer();
                    Console.WriteLine($"new customer generated {cust.Firstname} {cust.Lastname}");
                    var cont = JsonContent.Create(cust);
                    var res = await Client.PostAsync("http://localhost:5000/customers", cont);
                    if(res.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine($"server respond with status code {res.StatusCode}");
                    }
                    var body = await res.Content.ReadAsStringAsync();
                    res = await Client.GetAsync($"http://localhost:5000/customers/{body}");
                    var resp = await res.Content.ReadFromJsonAsync<Customer>();
                    var cs = await res.Content.ReadAsStringAsync();
                    var nc = JsonSerializer.Deserialize<Customer>(cs, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true});
                    Console.WriteLine($"user added id={resp.Id} name={resp.Firstname} lastname={resp.Lastname}");
                }
                else
                {
                    try
                    {
                        var id = Convert.ToInt32(rs);
                        handled= true;
                        try
                        {
                            var res = await Client.GetAsync($"http://localhost:5000/customers/{id}");
                            if (res.StatusCode != System.Net.HttpStatusCode.OK)
                            {
                                Console.WriteLine($"server respond with status code {res.StatusCode}");
                                continue;
                            }

                            Customer ncr = await res.Content.ReadFromJsonAsync<Customer>();
                            Console.WriteLine($"user found id={ncr.Id} name={ncr.Firstname} lastname={ncr.Lastname}");
                        }
                        catch(Exception ex) { Console.WriteLine(ex.Message); }
                    }
                    catch
                    {
                        handled = false;
                    };
                }
                if(!handled) Console.WriteLine(ts);
                
            }
        }

        

        private static Customer RandomCustomer()
        {
            string[] name = { "Семен","Василий","Саня","Андрей","Влад" };
            string[] sname = { "Перегей", "Козицин", "Шульгин", "Семенов", "Кледа" };
            string[] namef = { "Аня", "Лена", "Шура", "Вера", "Лара" };
            string[] snamef = { "Перегей", "Козицина", "Шульгина", "Семенова", "Кледа" };
            var g = new Random();
            var mf = g.Next(2);
            Customer ccr;
            if (0 == mf) ccr = new Customer() {Id= g.Next(200), Firstname= name[g.Next(5)], Lastname= sname[g.Next(5)] };
            else ccr = new Customer() { Id = g.Next(200), Firstname = namef[g.Next(5)], Lastname = snamef[g.Next(5)] };
            return ccr;
        }
    }
}